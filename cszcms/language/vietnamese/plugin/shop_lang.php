<?php
//Shop Index
$lang['shop_header']                    = "Hệ thống Cửa hàng";
$lang['shop_menu']                      = "Bảng điều khiển Cửa hàng";
$lang['shop_config']                    = "Cài đặt Cửa hàng";
$lang['shop_product_category']          = "Danh mục sản phẩm";
$lang['shop_products']                  = "Sản phẩm";
$lang['shop_order']                     = "Quản lý đơn hàng";
$lang['shop_stat_new']                  = "Sản phẩm mới";
$lang['shop_stat_hot']                  = "Sản phẩm hot";
$lang['shop_stat_bestseller']           = "Sản phẩm bán chạy";
$lang['shop_stat_soldout']              = "Sản phẩm bán hết";
$lang['shop_paymentrecent']             = "20 khoản thanh toán gần đây";
$lang['shop_dashboard_totalcomplete']   = "Tổng thanh toán hoàn tất";
$lang['shop_dashboard_totalorder']      = "Tổng đơn đặt hàng";
$lang['shop_dashboard_totalshipping']   = "Tổng vận chuyển";
$lang['shop_dashboard_totalproduct']   = "Tổng sản phẩm";
$lang['shop_allproducts']                  = "Tất cả sản phẩm";

// Shop Config
$lang['shop_config_header']             = "Cài đặt Cửa hàng";
$lang['shop_paypal_active']             = "Kích hoạt Paypal";
$lang['shop_sanbox_active']             = "Cho phép Paypal Sanbox";
$lang['shop_sanbox_remark']             = 'Paypal Sanbox: Đối với hệ thống kiểm tra thanh toán từ Paypal (<a href="https://developer.paypal.com/developer/accounts" target="_blank">More Help?</a>)';
$lang['shop_paypal_email']             = "Email Paypal doanh nghiệp";
$lang['shop_bank_detail']             = "Chi tiết ngân hàng";
$lang['shop_currency_code']             = "Mã tiền tệ";
$lang['shop_gst_vat']             = "Thuế GTGT(V.A.T) (*Zero is disable)";
$lang['shop_seller_email']             = "Email người bán";
$lang['shop_order_subject']             = "Tiêu đề email đơn hàng";
$lang['shop_order_body']                = "Nội dung email đơn hàng";
$lang['shop_payment_subject']             = "Tiêu đề email thanh toán";
$lang['shop_payment_body']             = "Nội dung email thanh toán";
$lang['shop_signature']                 = "Chữ ký email";
$lang['shop_email_header']                 = "Cài đặt email";
$lang['shop_payment_header']                 = "Cài đặt thanh toán";
$lang['shop_stat_new_show']                 = "Hiển thị sản phẩm mới";
$lang['shop_stat_hot_show']                 = "Hiển thị sản phẩm hot";
$lang['shop_stat_bestseller_show']                 = "Hiển thị sản phẩm bán chạy";
$lang['shop_stat_soldout_show']                 = "Hiển thị sản phẩm hết hàng";
$lang['shop_stat_remark']                 = "Chỉ dành cho trang chủ cửa hàng và Feed!";
$lang['shop_only_member']                 = "Chỉ cho thành viên thanh toán";
$lang['shop_bank_disable']                 = "Vô hiệu hóa chuyển khoản";
$lang['shop_exclude_gst_vat']                 = "Loại trừ Thuế GTGT(V.A.T)";
$lang['shop_paypal_fee']             = "Paypal fee (*Zero is disable)";
$lang['shop_decimals_config']               = "Show Decimals (0.00)";
$lang['shop_free_shipping_config']               = "Free Shipping when over amount (*Zero is disable)";
$lang['shop_line_notify_tokens']               = 'Line notify access tokens (<a href="https://notify-bot.line.me/my" target="_blank">More Help?</a>)';

// Shop Category
$lang['shop_category_header']             = "Danh mục sản phẩm";
$lang['shop_category_name']             = "Tên danh mục";
$lang['shop_cat_short_desc']                = "Mô tả ngắn";
$lang['shop_cat_keyword']                = "Từ khóa";
$lang['shop_active']                    = "Kích hoạt";
$lang['shop_category_addnew']             = "Thêm danh mục sản phẩm";
$lang['shop_category_edit']             = "Sửa danh mục sản phẩm";
$lang['shop_main_category']             = "Danh mục cha";

// Shop Products
$lang['shop_products_header']             = "Sản phẩm";
$lang['shop_products_name']             = "Tên sản phẩm";
$lang['shop_products_short_desc']                = "Mô tả ngắn";
$lang['shop_products_full_desc']                = "Mô tả chi tiết";
$lang['shop_products_keyword']                = "Từ khóa";
$lang['shop_products_price']                = "Giá tiền";
$lang['shop_products_discount']                = "Giảm giá";
$lang['shop_products_fullprice']                = "Giá gốc";
$lang['shop_products_stock']                = "Số lượng hàng";
$lang['shop_products_code']                = "Mã sản phẩm";
$lang['shop_products_photo']                = "Hình ảnh";
$lang['shop_products_status']                = "Trạng thái";
$lang['shop_products_addnew']             = "Thêm sản phẩm";
$lang['shop_products_edit']             = "Sửa sản phẩm";
$lang['shop_products_remark']           = "Tải lên hình ảnh và thêm tùy chọn sản phẩm. Vui lòng chỉnh sửa sản phẩm này sau khi thêm sản phẩm mới.";
$lang['shop_products_upload']                = "Tải ảnh lên";
$lang['shop_products_fileallow']              = "Chỉ cho phép các tệp (jpg, jpeg, png, gif). Kích thước không quá 1900px về chiều rộng hoặc chiều cao.";
$lang['shop_products_caption']                = "Chú thích";
$lang['shop_products_options']                = "Tùy chọn sản phẩm";

// Shop Order
$lang['shop_payment_status']             = "Tình trạng thanh toán";
$lang['shop_order_detail']             = "Chi tiết đơn hàng";
$lang['shop_payment_Completed']             = "Đã hoàn thành";
$lang['shop_payment_Pending']             = "Chờ xử lý";
$lang['shop_payment_Refunded']             = "Hoàn tiền";
$lang['shop_payment_Canceled']             = "Hủy bỏ";

// Shop Shipping
$lang['shop_shipping_header']             = "Giao vận";
$lang['shop_shipping_create']             = "Tạo vận chuyển";
$lang['shop_shipping_edit']             = "Sửa vận chuyển";
$lang['shop_shipping_name']             = "Tên vận chuyển";
$lang['shop_shipping_id']             = "ID vận chuyển";
$lang['shop_shipping_note']             = "Ghi chú";

// Shop promotion code
$lang['shop_promo_code']             = "Mã khuyến mại";
$lang['shop_promo_text']             = "Mã khuyến mại";
$lang['shop_promo_discount']             = "Giảm giá";
$lang['shop_promo_expired']            = "Khuyến mãi hết hạn";
$lang['shop_promo_addnew']             = "Thêm mã khuyến mãi";
$lang['shop_promo_edit']             = "Sửa mã khuyến mãi";

// Shop shipping cost
$lang['shop_shipping_cost_header']             = "Giá vận chuyển";
$lang['shop_shipping_cost_create']             = "Tạo phí vận chuyển";
$lang['shop_shipping_cost_edit']             = "Sửa phí vận chuyển";