<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <!-- Start Products Admin Menu -->
        <?php echo $this->Shop_model->AdminMenu() ?>
        <!-- End Products Admin Menu -->
        <ol class="breadcrumb">
            <li class="active">
                <i><span class="glyphicon glyphicon-edit"></span></i> <?php echo $this->lang->line('shop_shipping_cost_edit') ?>
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="h2 sub-header"><?php echo $this->lang->line('shop_shipping_cost_header') ?> <a role="button" href="<?php echo $this->Csz_model->base_link()?>/admin/plugin/shop/shipping_costNew" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-plus"></span> <?php echo  $this->lang->line('shop_shipping_cost_create') ?></a></div>
        <?php echo form_open($this->Csz_model->base_link() . '/admin/plugin/shop/shipping_costUpdate/'.$this->uri->segment(5)); ?>               
        <div class="control-group">	
            <?php echo form_error('shipping_name', '<div class="alert alert-danger text-center" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>'); ?>
            <label class="control-label" for="shipping_name"><?php echo $this->lang->line('shop_shipping_name'); ?>*</label>
            <?php
            $data = array(
                'name' => 'shipping_name',
                'id' => 'shipping_name',
                'required' => 'required',
                'autofocus' => 'true',
                'class' => 'form-control',
                'maxlength' => '255',
                'value' => set_value('shipping_name', $shipping_cost->shipping_name, FALSE)
            );
            echo form_input($data);
            ?>			
        </div> <!-- /control-group -->        
        <div class="control-group">
            <?php echo form_error('price', '<div class="alert alert-danger text-center" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>'); ?>
            <label class="control-label" for="price"><?php echo $this->lang->line('shop_products_price'); ?>*</label>
            <?php
                $data = array(
                    'name' => 'price',
                    'id' => 'price',
                    'required' => 'required',
                    'autofocus' => 'true',
                    'class' => 'form-control keypress-number',
                    'value' => set_value('price', $shipping_cost->price, FALSE)
                );
                echo form_input($data);
            ?>
        </div> <!-- /control-group -->
        <br>
        <div class="control-group">										
            <label class="form-control-static" for="active">
                <?php
                if ($shipping_cost->active) {
                    $checked = 'checked';
                } else {
                    $checked = '';
                }
                $data = array(
                    'name' => 'active',
                    'id' => 'active',
                    'value' => '1',
                    'checked' => $checked
                );
                echo form_checkbox($data);
                ?> <?php echo $this->lang->line('shop_active'); ?></label>	
        </div> <!-- /control-group -->
        <br><br>
        <div class="form-actions">
            <?php
            $data = array(
                'name' => 'submit',
                'id' => 'submit',
                'class' => 'btn btn-lg btn-primary',
                'value' => $this->lang->line('btn_save'),
            );
            echo form_submit($data);
            ?> 
            <a class="btn btn-lg" href="<?php echo $this->csz_referrer->getIndex('shop'); ?>"><?php echo $this->lang->line('btn_cancel'); ?></a>
        </div> <!-- /form-actions -->
        <?php echo form_close(); ?>
        <!-- /widget-content --> 
    </div>
</div>