/* For Version 1.1.0 */
DELETE FROM `general_label` WHERE 
`name` = 'shop_paypal_fee_txt' OR
`name` = 'shop_free_shipping_txt';
INSERT INTO `general_label` (`general_label_id`, `name`, `remark`, `lang_en`, `timestamp_update`) VALUES
('', 'shop_paypal_fee_txt', 'For paypal fee text', 'For Paypal. Include processing fee %d%%', NOW()),
('', 'shop_free_shipping_txt', 'For free shipping text', 'Free shipping when over amount %s', NOW());
ALTER TABLE `shop_config` ADD `paypal_fee` float NOT NULL AFTER `sanbox_active`;
ALTER TABLE `shop_config` ADD `decimals` int(11) NOT NULL AFTER `currency_code`;
ALTER TABLE `shop_config` ADD `line_notify_tokens` VARCHAR(255) NOT NULL AFTER `signature`;
ALTER TABLE `shop_config` ADD `free_shipping_amount` float NOT NULL AFTER `line_notify_tokens`;